class ClientController < ApplicationController

  def index
    @eats = Eat.all
    @foods = Food.all
  end

  def show_eats
    @eat = Eat.find(params[:id])
    @foods = @eat.foods.order(created_at: :desc)
  end

  def show_foods
    @food = Food.find(params[:id])
  end
end

class Admin::FoodsController < AdminController

  def index
    @foods = Food.all.order(created_at: :desc)
  end

  def show
    @food = Food.find(params[:id])
  end

  def new
    @food = Food.new
  end

  def create
    par = food_params
    par[:user_id] = current_user.id
    @food = Food.new(par)

    if @food.save
      flash[:success] = "Your food has been created successfully"
      redirect_to root_url
      else
      render 'new'
    end
  end

  def edit
    @food = Food.find(params[:id])
  end

  def destroy
  end

  def update
  end

private
  def food_params
    params.require(:food).permit(:id, :name, :price, :description, :eat_id)
  end

  def correct_user
    @food = Food.find(params[:id])
    if @food.user != current_user
      flash[:danger] = 'This is not your food.'
      redirect_to root_url
    end
  end
end

class Admin::EatsController < AdminController

  def index
    @eats = Eat.all
  end

  def show
    @eat = Eat.find(params[:id])
    @foods = @eat.foods.order(created_at: :desc)
  end

  def new
  end

  def edit
  end
end

class AdminController < ApplicationController
  layout 'admin'

  before_action :auth

  private

  def auth
    user = current_user

    if user.admin?
      authenticate_user!
      return true
    end

    redirect_to root_path
  end
end

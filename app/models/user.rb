class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  has_many :foods


  devise :database_authenticatable, :registerable,
         :rememberable, :trackable

  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: true
end

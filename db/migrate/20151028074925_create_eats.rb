class CreateEats < ActiveRecord::Migration
  def change
    create_table :eats do |t|
      t.string :name
      t.string :pic
      t.text :description

      t.timestamps null: false
    end
  end
end

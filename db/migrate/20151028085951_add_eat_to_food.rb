class AddEatToFood < ActiveRecord::Migration
  def change
    add_reference :foods, :eat, index: true, foreign_key: true
  end
end
